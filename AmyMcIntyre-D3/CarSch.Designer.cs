﻿namespace AmyMcIntyre_D3
{
    partial class CarSch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.instruct1 = new System.Windows.Forms.ComboBox();
            this.Cars = new System.Windows.Forms.Label();
            this.car1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Submit = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.load_button = new System.Windows.Forms.Button();
            this.listview = new System.Windows.Forms.ListView();
            this.Instructorname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.license = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.make = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(147, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(226, 39);
            this.label1.TabIndex = 43;
            this.label1.Text = "Car Schedule";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "Instructors";
            // 
            // instruct1
            // 
            this.instruct1.FormattingEnabled = true;
            this.instruct1.Location = new System.Drawing.Point(44, 154);
            this.instruct1.Name = "instruct1";
            this.instruct1.Size = new System.Drawing.Size(121, 21);
            this.instruct1.TabIndex = 45;
            // 
            // Cars
            // 
            this.Cars.AutoSize = true;
            this.Cars.Location = new System.Drawing.Point(263, 107);
            this.Cars.Name = "Cars";
            this.Cars.Size = new System.Drawing.Size(28, 13);
            this.Cars.TabIndex = 49;
            this.Cars.Text = "Cars";
            // 
            // car1
            // 
            this.car1.FormattingEnabled = true;
            this.car1.Items.AddRange(new object[] {
            "MAZDA",
            "FORD",
            "HOLDEN",
            "NISSAN",
            "TOYOTA"});
            this.car1.Location = new System.Drawing.Point(218, 154);
            this.car1.Name = "car1";
            this.car1.Size = new System.Drawing.Size(121, 21);
            this.car1.TabIndex = 50;
            this.car1.SelectedIndexChanged += new System.EventHandler(this.car1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(426, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 54;
            this.label3.Text = "License Plate";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(429, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 55;
            this.label4.Text = "label4";
            // 
            // Submit
            // 
            this.Submit.Location = new System.Drawing.Point(89, 420);
            this.Submit.Name = "Submit";
            this.Submit.Size = new System.Drawing.Size(122, 41);
            this.Submit.TabIndex = 59;
            this.Submit.Text = "Submit";
            this.Submit.UseVisualStyleBackColor = true;
            this.Submit.Click += new System.EventHandler(this.Submit_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(217, 420);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(122, 41);
            this.button2.TabIndex = 60;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // load_button
            // 
            this.load_button.Location = new System.Drawing.Point(371, 274);
            this.load_button.Name = "load_button";
            this.load_button.Size = new System.Drawing.Size(126, 23);
            this.load_button.TabIndex = 62;
            this.load_button.Text = "Load Info";
            this.load_button.UseVisualStyleBackColor = true;
            this.load_button.Click += new System.EventHandler(this.load_button_Click);
            // 
            // listview
            // 
            this.listview.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Instructorname,
            this.license,
            this.make});
            this.listview.Location = new System.Drawing.Point(12, 205);
            this.listview.Name = "listview";
            this.listview.Size = new System.Drawing.Size(327, 194);
            this.listview.TabIndex = 63;
            this.listview.UseCompatibleStateImageBehavior = false;
            // 
            // Instructorname
            // 
            this.Instructorname.Text = "Instructor Name";
            this.Instructorname.Width = 150;
            // 
            // license
            // 
            this.license.Text = "License";
            this.license.Width = 80;
            // 
            // make
            // 
            this.make.Text = "Make";
            this.make.Width = 80;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(371, 304);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 23);
            this.button1.TabIndex = 64;
            this.button1.Text = "Refresh";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(342, 420);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(122, 41);
            this.button3.TabIndex = 65;
            this.button3.Text = "Back";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // CarSch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 473);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listview);
            this.Controls.Add(this.load_button);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Submit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.car1);
            this.Controls.Add(this.Cars);
            this.Controls.Add(this.instruct1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "CarSch";
            this.Text = "CarSch";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox instruct1;
        private System.Windows.Forms.Label Cars;
        private System.Windows.Forms.ComboBox car1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Submit;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button load_button;
        private System.Windows.Forms.ListView listview;
        private System.Windows.Forms.ColumnHeader Instructorname;
        private System.Windows.Forms.ColumnHeader license;
        private System.Windows.Forms.ColumnHeader make;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
    }
}