﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmyMcIntyre_D3
{
    public partial class InstructorInfo : Form
    {
        public static string firstname;
        public static string lastname;
        public static string labeltext;
        public InstructorInfo(string CurrentUser)
        {
            InitializeComponent();
        }

        public void Block_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            this.Hide();
            InstructorTimeTable register = new InstructorTimeTable(firstname, lastname, labeltext);
            register.ShowDialog();
            this.Close();
        }
    }
}
