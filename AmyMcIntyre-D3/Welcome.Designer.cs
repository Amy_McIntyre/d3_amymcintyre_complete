﻿namespace AmyMcIntyre_D3
{
    partial class Welcome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ClientReg = new System.Windows.Forms.Button();
            this.ClientLogin = new System.Windows.Forms.Button();
            this.Instructorpage = new System.Windows.Forms.Button();
            this.Adminpage = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(81, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(309, 78);
            this.label1.TabIndex = 9;
            this.label1.Text = "     Welcome To \r\nThe Driving School";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(256, 26);
            this.label2.TabIndex = 10;
            this.label2.Text = "If you already have an account with us Log in below,\r\nOtherwise Click Register";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(192, 197);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(178, 20);
            this.textBoxPassword.TabIndex = 16;
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.Location = new System.Drawing.Point(192, 171);
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.Size = new System.Drawing.Size(178, 20);
            this.textBoxUserName.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(104, 197);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "Password:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(99, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "Username:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ClientReg
            // 
            this.ClientReg.Location = new System.Drawing.Point(183, 278);
            this.ClientReg.Name = "ClientReg";
            this.ClientReg.Size = new System.Drawing.Size(131, 23);
            this.ClientReg.TabIndex = 17;
            this.ClientReg.Text = "Register";
            this.ClientReg.UseVisualStyleBackColor = true;
            this.ClientReg.Click += new System.EventHandler(this.ClientReg_Click);
            // 
            // ClientLogin
            // 
            this.ClientLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClientLogin.Location = new System.Drawing.Point(160, 243);
            this.ClientLogin.Name = "ClientLogin";
            this.ClientLogin.Size = new System.Drawing.Size(178, 29);
            this.ClientLogin.TabIndex = 18;
            this.ClientLogin.Text = "Login";
            this.ClientLogin.UseVisualStyleBackColor = true;
            this.ClientLogin.Click += new System.EventHandler(this.ClientLogin_Click);
            // 
            // Instructorpage
            // 
            this.Instructorpage.Location = new System.Drawing.Point(12, 372);
            this.Instructorpage.Name = "Instructorpage";
            this.Instructorpage.Size = new System.Drawing.Size(118, 49);
            this.Instructorpage.TabIndex = 19;
            this.Instructorpage.Text = "Instructor";
            this.Instructorpage.UseVisualStyleBackColor = true;
            this.Instructorpage.Click += new System.EventHandler(this.Instructorpage_Click);
            // 
            // Adminpage
            // 
            this.Adminpage.Location = new System.Drawing.Point(342, 372);
            this.Adminpage.Name = "Adminpage";
            this.Adminpage.Size = new System.Drawing.Size(118, 49);
            this.Adminpage.TabIndex = 20;
            this.Adminpage.Text = "Admin";
            this.Adminpage.UseVisualStyleBackColor = true;
            this.Adminpage.Click += new System.EventHandler(this.Adminpage_Click);
            // 
            // Welcome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 433);
            this.Controls.Add(this.Adminpage);
            this.Controls.Add(this.Instructorpage);
            this.Controls.Add(this.ClientLogin);
            this.Controls.Add(this.ClientReg);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxUserName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Welcome";
            this.Text = "Driving School";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ClientReg;
        private System.Windows.Forms.Button ClientLogin;
        private System.Windows.Forms.Button Instructorpage;
        private System.Windows.Forms.Button Adminpage;
    }
}

