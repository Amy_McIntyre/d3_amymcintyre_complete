﻿namespace AmyMcIntyre_D3
{
    partial class BlockedOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.ClientName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Time = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Day = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.InstructorName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button1 = new System.Windows.Forms.Button();
            this.Approve_request = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ClientName,
            this.Time,
            this.Day,
            this.InstructorName});
            this.listView1.FullRowSelect = true;
            this.listView1.Location = new System.Drawing.Point(42, 86);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(568, 277);
            this.listView1.TabIndex = 6;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // ClientName
            // 
            this.ClientName.Text = "Client Name";
            this.ClientName.Width = 100;
            // 
            // Time
            // 
            this.Time.Text = "Time";
            this.Time.Width = 110;
            // 
            // Day
            // 
            this.Day.Text = "Day";
            this.Day.Width = 150;
            // 
            // InstructorName
            // 
            this.InstructorName.Text = "Instructor Name";
            this.InstructorName.Width = 100;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(242, 36);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Load Appointments";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Approve_request
            // 
            this.Approve_request.Location = new System.Drawing.Point(162, 392);
            this.Approve_request.Name = "Approve_request";
            this.Approve_request.Size = new System.Drawing.Size(121, 53);
            this.Approve_request.TabIndex = 8;
            this.Approve_request.Text = "Approve Request";
            this.Approve_request.UseVisualStyleBackColor = true;
            this.Approve_request.Click += new System.EventHandler(this.Approve_request_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(318, 392);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(131, 53);
            this.button2.TabIndex = 9;
            this.button2.Text = "Save and Exit";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // BlockedOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 476);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Approve_request);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listView1);
            this.Name = "BlockedOut";
            this.Text = "BlockedOut";
            this.Load += new System.EventHandler(this.BlockedOut_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader ClientName;
        private System.Windows.Forms.ColumnHeader Time;
        private System.Windows.Forms.ColumnHeader Day;
        private System.Windows.Forms.ColumnHeader InstructorName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Approve_request;
        private System.Windows.Forms.Button button2;
    }
}