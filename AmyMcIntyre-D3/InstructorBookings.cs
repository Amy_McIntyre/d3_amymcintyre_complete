﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AmyMcIntyre_D3
{

    public partial class InstructorBookings : Form
    {
        public static string timeslot;
        public static string day;
        public static string instructor;
        public static string clientname; 
        public static string fname;
        public static string firstname;
        public static string lastname;
        public static string lname;
        public static string labeltext;
        public InstructorBookings(string firstname, string lastname, string labeltext)
        {
            InitializeComponent();
            string fullname;
            fname = firstname;
            lname = lastname;
            fullname = $"{firstname} {lastname}";
            

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS308A-05A;Database=Deliverable2_AmyMcIntyre;Integrated Security=True")) //CHANGE THIS IF YOU HAVE DIFFERENT DATABASE NAME
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT FNAME, LNAME FROM INSTRUCTOR", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    instructor_box.Items.Add(sqlReader["FNAME"].ToString() + " " + (sqlReader["LNAME"].ToString()));
                }
                sqlReader.Close();
            }
        }

        public void submit_btn_Click(object sender, EventArgs e)
        {
            string clientname = "Blocked Out", time = "", date = "", instructorname = "" ;

            time = timeslot;
            date = Lesson_Date.Text;
            clientname = comboBox1.Text;
            instructorname = instructor_box.Text;

            string TimeI = "", DateI = "", InstructorNameI = "", CurrentUserI = ""; // TEST CODE

            TimeI = time;
            DateI = date;
            InstructorNameI = instructorname;
            CurrentUserI = clientname;

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS308A-05A;Database=Deliverable2_AmyMcIntyre; Integrated Security = True"))
            {
                SqlCommand sqlCmd = new SqlCommand($"SELECT * FROM TIMESLOT WHERE Clientname = '{CurrentUserI}' AND Time = '{TimeI}' AND Date = '{DateI}' AND InstructorName = '{InstructorNameI}'", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                while (sqlReader.Read())
                {
                    if (sqlReader.HasRows)
                    {
                        time = sqlReader.GetString(1);
                        date = sqlReader.GetString(2);
                        instructorname = sqlReader.GetString(3);
                        clientname = sqlReader.GetString(0);


                        if (clientname == "" && time == "" && date == "" && instructorname == "")
                        {

                            SQL.executeQuery("INSERT INTO TIMESLOT ( Clientname,Time, Date, InstructorName) VALUES ( '" + clientname + "','" + time + "', '" + date + "','" + instructorname + "')");
                            //success message for the user to know it worked
                            MessageBox.Show("Time successfully blocked out - Blocked out time is: " + date + " at " + time + " With " + instructorname);


                        }
                        else
                        {
                            MessageBox.Show("Time unavalible. Please Pick Another");
                            return;
                        }
                    }




                }

                sqlReader.Close();
            }
            try
            {
                SQL.executeQuery("INSERT INTO TIMESLOT (Clientname, Time, Date, InstructorName) VALUES ('" + clientname + "','" + time + "', '" + date + "','" + instructorname + "')");
                //success message for the user to know it worked
                MessageBox.Show("Time successfully blocked out - Blocked out time is: " + date + " at " + time + " With " + instructorname);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Time unavalible.");
                return;
            }
        }

        private void Lesson1_Click(object sender, EventArgs e)
        {
            timeslot = "9:00am-10:00am";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From:" + timeslot;
        }

        private void Lesson2_Click(object sender, EventArgs e)
        {
            timeslot = "10:00am-11:00am";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From:" + timeslot;
        }

        private void Lesson3_Click(object sender, EventArgs e)
        {
            timeslot = "11:00am-12:00pm";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From:" + timeslot;
        }

        private void Lesson4_Click(object sender, EventArgs e)
        {
            timeslot = "12:00pm-1:00pm";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From:" + timeslot;
        }

        private void Lesson5_Click(object sender, EventArgs e)
        {
            timeslot = "1:00pm-2:00pm";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From:" + timeslot;
        }

        private void Lesson6_Click(object sender, EventArgs e)
        {
            timeslot = "2:00pm-3:00pm";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From:" + timeslot;
        }

        private void Lesson7_Click(object sender, EventArgs e)
        {
            timeslot = "3:00pm-4:00pm";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From:" + timeslot;
        }

        private void Lesson8_Click(object sender, EventArgs e)
        {
            timeslot = "4:00pm-5:00pm";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From:" + timeslot;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            InstructorTimeTable register = new InstructorTimeTable(firstname, lastname, labeltext);
            register.ShowDialog();
            this.Close();
        }
    }
}
