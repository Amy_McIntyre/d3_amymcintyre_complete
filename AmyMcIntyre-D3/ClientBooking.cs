﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace AmyMcIntyre_D3
{
    public partial class ClientBooking : Form
    {
        public static string fname;
        public static string lname;
        public ClientBooking(string firstname, string lastname)
        {
            InitializeComponent();
            string fullname;
            fname = firstname;
            lname = lastname;
            fullname = $"{firstname} {lastname}";
            block.Text = fullname;

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS308A-05A;Database=Deliverable2_AmyMcIntyre;Integrated Security=True")) //CHANGE THIS IF YOU HAVE DIFFERENT DATABASE NAME
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT FNAME, LNAME FROM INSTRUCTOR", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    instructor_box.Items.Add(sqlReader["FNAME"].ToString() + " " + (sqlReader["LNAME"].ToString()));
                }
                sqlReader.Close();
            }
        }

        private void submit_btn_Click(object sender, EventArgs e)
        {
            string clientname = "", time = "", date = "", instructorname = "";

            time = timeslot;
            date = Lesson_Date.Text;
            clientname = block.Text;
            instructorname = instructor_box.Text;

            string TimeI = "", DateI = "", InstructorNameI = "", CurrentUserI = ""; // TEST CODE

            TimeI = time;
            DateI = date;
            InstructorNameI = instructorname;
            CurrentUserI = clientname;

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS308A-05A;Database=Deliverable2_AmyMcIntyre; Integrated Security = True"))
            {
                // System.Diagnostics.Debug.WriteLine($"########    SELECT * FROM Timeslot WHERE Time = '{TimeI}' AND Date = '{DateI}' AND InstructorName = '{InstructorNameI}' AND Client = '{CurrentUserI}'");


                SqlCommand sqlCmd = new SqlCommand($"SELECT * FROM TIMESLOT WHERE time = '{TimeI}' AND date = '{DateI}' AND instructorname = '{InstructorNameI}'", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                // name = SQL.read[2].ToString() + SQL.read[3].ToString();  (Time, Date, InstructorName, Client)

                while (sqlReader.Read())
                {
                    if (sqlReader.HasRows)
                    {
                        TimeI = sqlReader.GetString(1);
                        DateI = sqlReader.GetString(2);
                        InstructorNameI = sqlReader.GetString(3);
                        CurrentUserI = sqlReader.GetString(0);


                        if (TimeI == "" && DateI == "" && InstructorNameI == "" && CurrentUserI == "")
                        {
                            SQL.executeQuery("INSERT INTO TIMESLOT ( clientname, time, date, instructorname) VALUES ('" + clientname + "','" + time + "', '" + date + "','" + instructorname + "')");
                            //success message for the user to know it worked
                            MessageBox.Show("Appointment booked successfully - Your appointment is: " + date + " at " + time + " With " + instructorname);


                        }
                        else
                        {
                            MessageBox.Show("Time unavalible. Please Pick Another");
                            return;
                        }
                    }
                }

                sqlReader.Close();
            }

            try
            {
                SQL.executeQuery("INSERT INTO TIMESLOT VALUES ('" + clientname + "', '" + time + "', '" + date + "', '" + instructorname + "' )"); 
                TextWriter txt = new StreamWriter("mail.txt");
                txt.Write("Hi " + block.Text + ". \r\nThis is an email confirming your booking with us. We are happy you chose to learn with us." +
                    "\r\nThe Time of your appointment is: " + timeslot + " " + "\r\nThe Date of your appointment is: " + Lesson_Date.Text + " " +
                    "\r\nThe Instructor you chose for this lesson is: " + instructor_box.Text + " " + "\r\nWe hope you have a good lesson with our instructors and learn a lot. " +
                    "\r\nKind Regards," + "\r\nThe Driving School ");
                txt.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }
        }

        public static string timeslot;
        public static string day;
        public static string instructor;
        public static string client;

        private void first_lesson_Click(object sender, EventArgs e)
        {
            timeslot = "9:00am - 10:00am";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From: " + timeslot;
        }

        private void second_lesson_Click(object sender, EventArgs e)
        {
            timeslot = "10:00am - 11:00am";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From: " + timeslot;
        }

        private void thrid_lesson_Click(object sender, EventArgs e)
        {
            timeslot = "11:00am - 12:00pm";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From: " + timeslot;
        }

        private void fourth_lesson_Click(object sender, EventArgs e)
        {
            timeslot = "12:00pm - 1:00pm";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From: " + timeslot;
        }

        private void fifth_lesson_Click(object sender, EventArgs e)
        {
            timeslot = "1:00pm - 2:00pm";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From: " + timeslot;
        }

        private void sixth_lesson_Click(object sender, EventArgs e)
        {
            timeslot = "2:00pm - 3:00pm";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From: " + timeslot;
        }

        private void seventh_lesson_Click(object sender, EventArgs e)
        {
            timeslot = "3:00pm - 4:00pm";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From: " + timeslot;
        }

        private void eighth_lesson_Click(object sender, EventArgs e)
        {
            timeslot = "4:00pm - 5:00pm";
            LessonInfo.Text = Lesson_Date.Text.ToString() + "From: " + timeslot;
        }

        private void Lesson_Date_ValueChanged(object sender, EventArgs e)
        {
            if (Lesson_Date.Text.Contains("Sunday"))
            {
                MessageBox.Show("Sorry, We are not open on Sundays. Please choose another appointment from Monday-Saturday");
                Lesson_Date.Text = Lesson_Date.Value.AddDays(1).ToString();

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Welcome register = new Welcome();
            register.ShowDialog();
            this.Close();
        }
    }
}
