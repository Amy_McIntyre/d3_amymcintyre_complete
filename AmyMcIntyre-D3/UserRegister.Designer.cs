﻿namespace AmyMcIntyre_D3
{
    partial class UserRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cost_box = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.experiencebox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.phone_box = new System.Windows.Forms.TextBox();
            this.email_box = new System.Windows.Forms.TextBox();
            this.lastname_box = new System.Windows.Forms.TextBox();
            this.firstname_box = new System.Windows.Forms.TextBox();
            this.password_box = new System.Windows.Forms.TextBox();
            this.username_box = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.time_box = new System.Windows.Forms.Label();
            this.ClientReg = new System.Windows.Forms.Button();
            this.Back_button = new System.Windows.Forms.Button();
            this.LoinPage_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cost_box
            // 
            this.cost_box.AutoSize = true;
            this.cost_box.Location = new System.Drawing.Point(235, 364);
            this.cost_box.Name = "cost_box";
            this.cost_box.Size = new System.Drawing.Size(13, 13);
            this.cost_box.TabIndex = 41;
            this.cost_box.Text = "$";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(139, 398);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 16);
            this.label10.TabIndex = 40;
            this.label10.Text = "Hours:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(136, 361);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 16);
            this.label9.TabIndex = 39;
            this.label9.Text = "Cost:";
            // 
            // experiencebox
            // 
            this.experiencebox.FormattingEnabled = true;
            this.experiencebox.Items.AddRange(new object[] {
            "Experienced",
            "Learner"});
            this.experiencebox.Location = new System.Drawing.Point(249, 321);
            this.experiencebox.Name = "experiencebox";
            this.experiencebox.Size = new System.Drawing.Size(151, 21);
            this.experiencebox.TabIndex = 38;
            this.experiencebox.SelectedIndexChanged += new System.EventHandler(this.experiencebox_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(136, 321);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 16);
            this.label8.TabIndex = 37;
            this.label8.Text = "Driving Level:";
            // 
            // phone_box
            // 
            this.phone_box.Location = new System.Drawing.Point(249, 288);
            this.phone_box.Name = "phone_box";
            this.phone_box.Size = new System.Drawing.Size(151, 20);
            this.phone_box.TabIndex = 36;
            // 
            // email_box
            // 
            this.email_box.Location = new System.Drawing.Point(249, 250);
            this.email_box.Name = "email_box";
            this.email_box.Size = new System.Drawing.Size(151, 20);
            this.email_box.TabIndex = 35;
            // 
            // lastname_box
            // 
            this.lastname_box.Location = new System.Drawing.Point(249, 214);
            this.lastname_box.Name = "lastname_box";
            this.lastname_box.Size = new System.Drawing.Size(151, 20);
            this.lastname_box.TabIndex = 34;
            // 
            // firstname_box
            // 
            this.firstname_box.Location = new System.Drawing.Point(249, 174);
            this.firstname_box.Name = "firstname_box";
            this.firstname_box.Size = new System.Drawing.Size(151, 20);
            this.firstname_box.TabIndex = 33;
            // 
            // password_box
            // 
            this.password_box.Location = new System.Drawing.Point(249, 133);
            this.password_box.Name = "password_box";
            this.password_box.Size = new System.Drawing.Size(151, 20);
            this.password_box.TabIndex = 32;
            // 
            // username_box
            // 
            this.username_box.Location = new System.Drawing.Point(249, 92);
            this.username_box.Name = "username_box";
            this.username_box.Size = new System.Drawing.Size(151, 20);
            this.username_box.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label7.Location = new System.Drawing.Point(136, 288);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 17);
            this.label7.TabIndex = 30;
            this.label7.Text = "Mobile Phone:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label6.Location = new System.Drawing.Point(136, 250);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 17);
            this.label6.TabIndex = 29;
            this.label6.Text = "Email:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label5.Location = new System.Drawing.Point(136, 214);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 17);
            this.label5.TabIndex = 28;
            this.label5.Text = "Last Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label4.Location = new System.Drawing.Point(136, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 17);
            this.label4.TabIndex = 27;
            this.label4.Text = "First Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label3.Location = new System.Drawing.Point(136, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 26;
            this.label3.Text = "Password:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.Location = new System.Drawing.Point(136, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 17);
            this.label2.TabIndex = 25;
            this.label2.Text = "Username:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(157, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(230, 39);
            this.label1.TabIndex = 42;
            this.label1.Text = "User Register";
            // 
            // time_box
            // 
            this.time_box.AutoSize = true;
            this.time_box.Location = new System.Drawing.Point(235, 398);
            this.time_box.Name = "time_box";
            this.time_box.Size = new System.Drawing.Size(77, 13);
            this.time_box.TabIndex = 43;
            this.time_box.Text = "Allocated Time";
            // 
            // ClientReg
            // 
            this.ClientReg.Location = new System.Drawing.Point(212, 472);
            this.ClientReg.Name = "ClientReg";
            this.ClientReg.Size = new System.Drawing.Size(131, 23);
            this.ClientReg.TabIndex = 44;
            this.ClientReg.Text = "Register";
            this.ClientReg.UseVisualStyleBackColor = true;
            this.ClientReg.Click += new System.EventHandler(this.ClientReg_Click);
            // 
            // Back_button
            // 
            this.Back_button.Location = new System.Drawing.Point(31, 472);
            this.Back_button.Name = "Back_button";
            this.Back_button.Size = new System.Drawing.Size(117, 23);
            this.Back_button.TabIndex = 45;
            this.Back_button.Text = "Back";
            this.Back_button.UseVisualStyleBackColor = true;
            this.Back_button.Click += new System.EventHandler(this.Back_button_Click);
            // 
            // LoinPage_button
            // 
            this.LoinPage_button.Location = new System.Drawing.Point(390, 472);
            this.LoinPage_button.Name = "LoinPage_button";
            this.LoinPage_button.Size = new System.Drawing.Size(117, 23);
            this.LoinPage_button.TabIndex = 46;
            this.LoinPage_button.Text = "Login Page";
            this.LoinPage_button.UseVisualStyleBackColor = true;
            this.LoinPage_button.Click += new System.EventHandler(this.LoinPage_button_Click);
            // 
            // UserRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 507);
            this.Controls.Add(this.LoinPage_button);
            this.Controls.Add(this.Back_button);
            this.Controls.Add(this.ClientReg);
            this.Controls.Add(this.time_box);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cost_box);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.experiencebox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.phone_box);
            this.Controls.Add(this.email_box);
            this.Controls.Add(this.lastname_box);
            this.Controls.Add(this.firstname_box);
            this.Controls.Add(this.password_box);
            this.Controls.Add(this.username_box);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "UserRegister";
            this.Text = "UserRegister";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label cost_box;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox experiencebox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox phone_box;
        private System.Windows.Forms.TextBox email_box;
        private System.Windows.Forms.TextBox lastname_box;
        private System.Windows.Forms.TextBox firstname_box;
        private System.Windows.Forms.TextBox password_box;
        private System.Windows.Forms.TextBox username_box;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label time_box;
        private System.Windows.Forms.Button ClientReg;
        private System.Windows.Forms.Button Back_button;
        private System.Windows.Forms.Button LoinPage_button;
    }
}