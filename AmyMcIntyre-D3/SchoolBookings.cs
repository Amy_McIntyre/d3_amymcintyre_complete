﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AmyMcIntyre_D3
{
    public partial class SchoolBookings : Form
    {
        public SchoolBookings()
        {
            InitializeComponent();
        }

        void stuff(string fname)
        {
            con.Open();
            cmd.CommandText = "select * from TIMESLOT";
            cmd.Connection = con;
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                listView1.View = View.Details;


                ListViewItem item = new ListViewItem(dr["clientname"].ToString());
                item.SubItems.Add(dr["time"].ToString());
                item.SubItems.Add(dr["date"].ToString());
                item.SubItems.Add(dr["instructorname"].ToString());

                listView1.Items.Add(item);
            }
            con.Close();
        }
        public static string fname;
        public static string lname;
        public static string firstname;
        public static string lastname;

        SqlConnection con = new SqlConnection(@"Data Source = BCS308A-05A; Database = Deliverable2_AmyMcIntyre; Integrated Security = True");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader dr;

        private void Load_Apps_Click(object sender, EventArgs e)
        {
            stuff(fname);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminScheduler register = new AdminScheduler(firstname, lastname);
            register.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            CarSch register = new CarSch(firstname, lastname);
            register.ShowDialog();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            BlockedOut register = new BlockedOut();
            register.ShowDialog();
            this.Close();
        }

        private void SchoolBookings_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Welcome register = new Welcome();
            register.ShowDialog();
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            EditUsers register = new EditUsers();
            register.ShowDialog();
            this.Close();
        }
    }
}
