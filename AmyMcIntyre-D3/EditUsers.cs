﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AmyMcIntyre_D3
{

    public partial class EditUsers : Form
    {
        public static string firstname;
        public static string fname;
        public static string lname;
        public static string lastname;
        public static string labeltext;
        public EditUsers()
        {

            InitializeComponent();
            string fullname;
            fname = firstname;
            lname = lastname;
            fullname = $"{firstname} {lastname}";

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS308A-05A;Database=Deliverable2_AmyMcIntyre;Integrated Security=True")) //CHANGE THIS IF YOU HAVE DIFFERENT DATABASE NAME
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT FNAME, LNAME FROM INSTRUCTOR", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    comboBox1.Items.Add(sqlReader["FNAME"].ToString() + " " + (sqlReader["LNAME"].ToString()));
                }
                sqlReader.Close();
            }
        }

        

        private void Remove_Admin_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                //using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS308A-05A;Database=Deliverable2_AmyMcIntyre;Integrated Security=True")) //CHANGE THIS IF YOU HAVE DIFFERENT DATABASE NAME
                //{
                //    SqlCommand sqlCmd = new SqlCommand("SELECT FNAME FROM ADMIN", sqlConnection);
                //    sqlConnection.Open();
                //    SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                //    while (sqlReader.Read())
                //    {
                //        checkBox1.Items.Add(sqlReader["FNAME"].ToString());
                //    }
                //    sqlReader.Close();
                //}
                SQL.executeQuery($"DELETE FROM ADMIN WHERE FNAME = '{fname}'");
                new Welcome().Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Do you wish to delete this account? Please tick the checkbox and click submit again");
            }
        }

        private void Remove_instruct_Click(object sender, EventArgs e)
        {
            SQL.executeQuery($"DELETE FROM INSTRUCTOR WHERE FNAME = '{fname}'");
            MessageBox.Show("Instructor has been successfuly deleted");
        }
    }
}
