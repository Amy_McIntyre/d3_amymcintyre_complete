﻿namespace AmyMcIntyre_D3
{
    partial class InstructorLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPass = new System.Windows.Forms.TextBox();
            this.textBoxUser = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.InstructLogin = new System.Windows.Forms.Button();
            this.InstructReg = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(118, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(256, 39);
            this.label1.TabIndex = 43;
            this.label1.Text = "Instructor Login";
            // 
            // textBoxPass
            // 
            this.textBoxPass.Location = new System.Drawing.Point(205, 176);
            this.textBoxPass.Name = "textBoxPass";
            this.textBoxPass.Size = new System.Drawing.Size(140, 20);
            this.textBoxPass.TabIndex = 47;
            // 
            // textBoxUser
            // 
            this.textBoxUser.Location = new System.Drawing.Point(205, 128);
            this.textBoxUser.Name = "textBoxUser";
            this.textBoxUser.Size = new System.Drawing.Size(140, 20);
            this.textBoxUser.TabIndex = 46;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label3.Location = new System.Drawing.Point(88, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 45;
            this.label3.Text = "Password:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.Location = new System.Drawing.Point(88, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 17);
            this.label2.TabIndex = 44;
            this.label2.Text = "Username:";
            // 
            // InstructLogin
            // 
            this.InstructLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstructLogin.Location = new System.Drawing.Point(167, 234);
            this.InstructLogin.Name = "InstructLogin";
            this.InstructLogin.Size = new System.Drawing.Size(178, 29);
            this.InstructLogin.TabIndex = 49;
            this.InstructLogin.Text = "Login";
            this.InstructLogin.UseVisualStyleBackColor = true;
            this.InstructLogin.Click += new System.EventHandler(this.InstructLogin_Click);
            // 
            // InstructReg
            // 
            this.InstructReg.Location = new System.Drawing.Point(190, 269);
            this.InstructReg.Name = "InstructReg";
            this.InstructReg.Size = new System.Drawing.Size(131, 23);
            this.InstructReg.TabIndex = 48;
            this.InstructReg.Text = "Register";
            this.InstructReg.UseVisualStyleBackColor = true;
            this.InstructReg.Click += new System.EventHandler(this.InstructReg_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(190, 407);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 23);
            this.button1.TabIndex = 50;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // InstructorLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 442);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.InstructLogin);
            this.Controls.Add(this.InstructReg);
            this.Controls.Add(this.textBoxPass);
            this.Controls.Add(this.textBoxUser);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "InstructorLogin";
            this.Text = "InstructorLogin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPass;
        private System.Windows.Forms.TextBox textBoxUser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button InstructLogin;
        private System.Windows.Forms.Button InstructReg;
        private System.Windows.Forms.Button button1;
    }
}