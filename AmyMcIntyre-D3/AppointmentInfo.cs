﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace AmyMcIntyre_D3
{
    public partial class AppointmentInfo : Form
    {
        public static string firstname;
        public static string fname;
        public static string lname;
        public static string lastname;
        public static string labeltext;

        public AppointmentInfo()
        {
            InitializeComponent();
            string fullname;
            fname = firstname;
            lname = lastname;
            fullname = $"{firstname} {lastname}";

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS308A-05A;Database=Deliverable2_AmyMcIntyre;Integrated Security=True")) //CHANGE THIS IF YOU HAVE DIFFERENT DATABASE NAME
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT FNAME, LNAME FROM INSTRUCTOR", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    instructorname.Items.Add(sqlReader["FNAME"].ToString() + " " + (sqlReader["LNAME"].ToString()));
                }
                sqlReader.Close();
            }

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS308A-05A;Database=Deliverable2_AmyMcIntyre;Integrated Security=True")) //CHANGE THIS IF YOU HAVE DIFFERENT DATABASE NAME
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT FNAME, LNAME FROM CLIENT", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    Clientname.Items.Add(sqlReader["FNAME"].ToString() + " " + (sqlReader["LNAME"].ToString()));
                }
                sqlReader.Close();
            }

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS308A-05A;Database=Deliverable2_AmyMcIntyre;Integrated Security=True")) //CHANGE THIS IF YOU HAVE DIFFERENT DATABASE NAME
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT DATE FROM TIMESLOT", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    date.Items.Add(sqlReader["DATE"].ToString());
                }
                sqlReader.Close();
            }

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS308A-05A;Database=Deliverable2_AmyMcIntyre;Integrated Security=True")) //CHANGE THIS IF YOU HAVE DIFFERENT DATABASE NAME
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT TIME FROM TIMESLOT", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    time.Items.Add(sqlReader["TIME"].ToString());
                }
                sqlReader.Close();
            }
        }

        private void save_Click(object sender, EventArgs e)
        {
            string clientname = "", time1 = "", date1 = "", instructorname1 = "", notes1 = "", experience1 = "", cost1 = "", hours1 = "", confirmed1 = "";
            clientname = Clientname.Text;
            time1 = time.Text;
            date1 = date.Text;
            instructorname1 = instructorname.Text;
            notes1 = textBox1.Text;
            experience1 = comboBox1.Text;
            hours1 = label12.Text;
            cost1 = label11.Text;
            confirmed1 = confirmation.Text;

            string clientnameA = "", time1A = "", date1A = "", instructorname1A = "", notes1A = "", experience1A = "", cost1A = "", hours1A = "", confirmed1A = "";

            clientnameA = clientname;
            time1A = time1;
            date1A = date1;
            instructorname1A = instructorname1;
            notes1A = notes1;
            confirmed1A = confirmed1;
            experience1A = experience1;
            hours1A = hours1;
            cost1A = cost1;

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS308A-05A;Database=Deliverable2_AmyMcIntyre; Integrated Security = True"))
            {
                // System.Diagnostics.Debug.WriteLine($"########    SELECT * FROM Timeslot WHERE Time = '{TimeI}' AND Date = '{DateI}' AND InstructorName = '{InstructorNameI}' AND Client = '{CurrentUserI}'");


                SqlCommand sqlCmd = new SqlCommand($"SELECT * FROM APPOINTMENT WHERE clientname = '{clientnameA}' AND time = '{time1A}' AND date = '{date1A}' AND instructorname = '{instructorname1A}' AND notes = '{notes1A}' AND experience = '{experience1A}' AND clientcost = '{cost1A}' AND clienthours = '{hours1A}' AND confirmed = '{confirmed1A}'", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                // name = SQL.read[2].ToString() + SQL.read[3].ToString();  (Time, Date, InstructorName, Client)

                while (sqlReader.Read())
                {
                    if (sqlReader.HasRows)
                    {
                        time1 = sqlReader.GetString(1);
                        date1 = sqlReader.GetString(2);
                        instructorname1 = sqlReader.GetString(3);
                        notes1 = sqlReader.GetString(4);
                        confirmed1 = sqlReader.GetString(8);
                        clientname = sqlReader.GetString(0);
                        experience1 = sqlReader.GetString(5);
                        cost1 = sqlReader.GetString(6);
                        hours1 = sqlReader.GetString(7);


                        if (time1 == "" && date1 == "" && instructorname1 == "" && clientname == "" && experience1 == "" && cost1 == "" && hours1 == "")
                        {
                            SQL.executeQuery("INSERT INTO APPOINTMENT ( clientname, time, date, instructorname, notes, experience, clientcost, clienthours, confirmed) VALUES ('" + clientname + "','" + time1 + "', '" + date1 + "','" + instructorname1 + "', '" + notes1 + "','" + experience1 + "','" + cost1 + "','" + hours1 + "', '" + confirmed1 + "' )");
                            //success message for the user to know it worked
                            System.Diagnostics.Debug.WriteLine("Something");
                            MessageBox.Show("Appointment booked successfully - Your appointment is: " + date + " at " + time + " With " + instructorname);


                        }
                        else
                        {
                            MessageBox.Show("Time unavalible. Please Pick Another");
                            return;
                        }
                    }
                }

                sqlReader.Close();
            }

            try
            {
                SQL.executeQuery("INSERT INTO APPOINTMENT (clientname, time, date, instructorname, notes, experience, clientcost, clienthours, confirmed) VALUES ('" + clientname + "', '" + time1 + "', '" + date1 + "','" + instructorname1 + "', '" + notes1 + "','" + experience1 + "','" + cost1 + "','" + hours1 + "', '" + confirmed1 + "' )");
                //success message for the user to know it worked
                MessageBox.Show("Appointment Sheet Saved");

                TextWriter txt = new StreamWriter("bill.txt");
                txt.Write("Hi "+Clientname.Text+ " " + "\r\n We are just emailing this to you as it is a summary of hour you lesson went and a bill for the lesson" +
                    "\r\n The time of your lesson was "+time.Text+ " \r\n  The date of your lesson was "+date.Text+"" +
                    "\r\n The instructor you chose for this lesson was "+instructorname.Text+"" +
                    "\r\n The Notes your Instructor things you need to work and/or Improve on are as follows \r\n "+textBox1.Text+"" +
                    "\r\n \r\n Your experiecne level you are at is "+comboBox1.Text+"" +
                    "\r\n Which brings the total cost of your lessons to "+label11.Text+" and the hours you need to drive are "+label12.Text+""+
                    "\r\n Hope you enjoyed your lesson with us" +
                    "\r\n \r\n Kind Regards \r\n The Driving School");
                txt.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Time unavalible.");
                return;
            }

        }

        private void back_Click(object sender, EventArgs e)
        {
            this.Hide();
            InstructorTimeTable register = new InstructorTimeTable(firstname, lastname, labeltext);
            register.ShowDialog();
            this.Close();
        }

        private void Clientname_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((string)comboBox1.SelectedItem == "Experienced")
            {
                label11.Text = "95.00";
                label12.Text = "2";
            }
            else
            {
                label11.Text = "195.00";
                label12.Text = "5";
            }
        }
    }
}
