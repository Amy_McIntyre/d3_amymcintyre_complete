﻿namespace AmyMcIntyre_D3
{
    partial class InstructorTimeTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.delete_records = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.ClientName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Time = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Day = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.InstructorName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Approve = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.update_records = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // delete_records
            // 
            this.delete_records.Location = new System.Drawing.Point(220, 403);
            this.delete_records.Name = "delete_records";
            this.delete_records.Size = new System.Drawing.Size(187, 39);
            this.delete_records.TabIndex = 5;
            this.delete_records.Text = "LogOut";
            this.delete_records.UseVisualStyleBackColor = true;
            this.delete_records.Click += new System.EventHandler(this.delete_records_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(210, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(197, 42);
            this.button1.TabIndex = 4;
            this.button1.Text = "Load Appointments";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ClientName,
            this.Time,
            this.Day,
            this.InstructorName,
            this.Approve});
            this.listView1.Location = new System.Drawing.Point(32, 100);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(568, 254);
            this.listView1.TabIndex = 3;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // ClientName
            // 
            this.ClientName.Text = "Client Name";
            this.ClientName.Width = 100;
            // 
            // Time
            // 
            this.Time.Text = "Time";
            this.Time.Width = 110;
            // 
            // Day
            // 
            this.Day.Text = "Day";
            this.Day.Width = 150;
            // 
            // InstructorName
            // 
            this.InstructorName.Text = "Instructor Name";
            this.InstructorName.Width = 100;
            // 
            // Approve
            // 
            this.Approve.Text = "Approve";
            // 
            // update_records
            // 
            this.update_records.Location = new System.Drawing.Point(7, 403);
            this.update_records.Name = "update_records";
            this.update_records.Size = new System.Drawing.Size(187, 39);
            this.update_records.TabIndex = 6;
            this.update_records.Text = "Block out appointments";
            this.update_records.UseVisualStyleBackColor = true;
            this.update_records.Click += new System.EventHandler(this.update_records_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(433, 403);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(187, 39);
            this.button2.TabIndex = 8;
            this.button2.Text = "Confirm Appointments";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // InstructorTimeTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 482);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.update_records);
            this.Controls.Add(this.delete_records);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listView1);
            this.Name = "InstructorTimeTable";
            this.Text = "TimeTable";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button delete_records;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader ClientName;
        private System.Windows.Forms.ColumnHeader Time;
        private System.Windows.Forms.ColumnHeader Day;
        private System.Windows.Forms.ColumnHeader InstructorName;
        private System.Windows.Forms.Button update_records;
        private System.Windows.Forms.ColumnHeader Approve;
        private System.Windows.Forms.Button button2;
    }
}