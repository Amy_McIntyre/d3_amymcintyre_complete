﻿namespace AmyMcIntyre_D3
{
    partial class EditUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.Remove_Admin = new System.Windows.Forms.Button();
            this.Remove_instruct = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(126, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(248, 39);
            this.label6.TabIndex = 64;
            this.label6.Text = "Remove Users";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 13);
            this.label1.TabIndex = 65;
            this.label1.Text = "Remove Instructor Account";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(189, 109);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(172, 21);
            this.comboBox1.TabIndex = 66;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(11, 189);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(151, 17);
            this.checkBox1.TabIndex = 67;
            this.checkBox1.Text = "Delete Current Admin User";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // Remove_Admin
            // 
            this.Remove_Admin.Location = new System.Drawing.Point(189, 189);
            this.Remove_Admin.Name = "Remove_Admin";
            this.Remove_Admin.Size = new System.Drawing.Size(105, 23);
            this.Remove_Admin.TabIndex = 68;
            this.Remove_Admin.Text = "Submit";
            this.Remove_Admin.UseVisualStyleBackColor = true;
            this.Remove_Admin.Click += new System.EventHandler(this.Remove_Admin_Click);
            // 
            // Remove_instruct
            // 
            this.Remove_instruct.Location = new System.Drawing.Point(387, 107);
            this.Remove_instruct.Name = "Remove_instruct";
            this.Remove_instruct.Size = new System.Drawing.Size(105, 23);
            this.Remove_instruct.TabIndex = 69;
            this.Remove_instruct.Text = "Submit";
            this.Remove_instruct.UseVisualStyleBackColor = true;
            this.Remove_instruct.Click += new System.EventHandler(this.Remove_instruct_Click);
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(387, 247);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(105, 23);
            this.Back.TabIndex = 70;
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            // 
            // EditUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 282);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.Remove_instruct);
            this.Controls.Add(this.Remove_Admin);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label6);
            this.Name = "EditUsers";
            this.Text = "EditUsers";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button Remove_Admin;
        private System.Windows.Forms.Button Remove_instruct;
        private System.Windows.Forms.Button Back;
    }
}