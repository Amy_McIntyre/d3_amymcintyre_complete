﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AmyMcIntyre_D3
{
    public partial class BlockedOut : Form
    {
        public BlockedOut()
        {
            InitializeComponent();
        }

         void Update(string updatename)
        {
            con.Open();
            cmd.CommandText = "UPDATE TIMESLOT SET CLIENTNAME = '"+ updatename +"' WHERE CLIENTNAME = 'Blocked Out' ";
            cmd.Connection = con;
            dr = cmd.ExecuteReader();
            
        }
        public static string checkbox;
        public static string Blocked;
        public static string updatename;
        public static string labeltext;
        public static string lname;
        public static string firstname;
        public static string lastname;

        void stuff(string Blocked)
        {
            con.Open();
            cmd.CommandText = "select * from TIMESLOT where CLIENTNAME like '%" + "Blocked Out" + "%'";
            cmd.Connection = con;
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                listView1.View = View.Details;
                
                ListViewItem item = new ListViewItem(dr["clientname"].ToString());
                item.SubItems.Add(dr["time"].ToString());
                item.SubItems.Add(dr["date"].ToString());
                item.SubItems.Add(dr["instructorname"].ToString());
                listView1.Items.Add(item);
            }
            con.Close();
        }


        SqlConnection con = new SqlConnection(@"Data Source = BCS308A-05A; Database = Deliverable2_AmyMcIntyre; Integrated Security = True");
        SqlCommand cmd = new SqlCommand();
        
        SqlDataReader dr;

        private void BlockedOut_Load(object sender, EventArgs e)
        {


        }

        private void button1_Click(object sender, EventArgs e)
        {
            stuff(Blocked);
        }

        private void Approve_request_Click(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                DialogResult result = MessageBox.Show("Select yes or No.", "Yes or No", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                if (result == DialogResult.Yes)
                {
                    ListViewItem item = new ListViewItem();
                    listView1.Items.Add(item);
                    listView1.Items[0].Text = "Approved";
                    Update(updatename = "Approved");

                }
                else if (result == DialogResult.No)
                {
                    MessageBox.Show("You chose No.");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            


            this.Hide();
            SchoolBookings register = new SchoolBookings();
            register.ShowDialog();
            this.Close();
        }
    }
}
