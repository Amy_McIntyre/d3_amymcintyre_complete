﻿namespace AmyMcIntyre_D3
{
    partial class ClientBooking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.fourth_lesson = new System.Windows.Forms.Button();
            this.second_lesson = new System.Windows.Forms.Button();
            this.thrid_lesson = new System.Windows.Forms.Button();
            this.first_lesson = new System.Windows.Forms.Button();
            this.Lesson_Date = new System.Windows.Forms.DateTimePicker();
            this.submit_btn = new System.Windows.Forms.Button();
            this.instructor_box = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.eighth_lesson = new System.Windows.Forms.Button();
            this.sixth_lesson = new System.Windows.Forms.Button();
            this.seventh_lesson = new System.Windows.Forms.Button();
            this.fifth_lesson = new System.Windows.Forms.Button();
            this.LessonInfo = new System.Windows.Forms.Label();
            this.block = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(159, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(249, 39);
            this.label1.TabIndex = 10;
            this.label1.Text = "Book A Lesson";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(93, 318);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 56;
            this.label4.Text = "Appointment: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(256, 183);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 55;
            this.label3.Text = "Pick a time";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(89, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 54;
            this.label2.Text = "Choose a date";
            // 
            // fourth_lesson
            // 
            this.fourth_lesson.Location = new System.Drawing.Point(384, 209);
            this.fourth_lesson.Name = "fourth_lesson";
            this.fourth_lesson.Size = new System.Drawing.Size(94, 39);
            this.fourth_lesson.TabIndex = 53;
            this.fourth_lesson.Text = "12pm-1pm";
            this.fourth_lesson.UseVisualStyleBackColor = true;
            this.fourth_lesson.Click += new System.EventHandler(this.fourth_lesson_Click);
            // 
            // second_lesson
            // 
            this.second_lesson.Location = new System.Drawing.Point(184, 209);
            this.second_lesson.Name = "second_lesson";
            this.second_lesson.Size = new System.Drawing.Size(94, 39);
            this.second_lesson.TabIndex = 52;
            this.second_lesson.Text = "10am-11am";
            this.second_lesson.UseVisualStyleBackColor = true;
            this.second_lesson.Click += new System.EventHandler(this.second_lesson_Click);
            // 
            // thrid_lesson
            // 
            this.thrid_lesson.Location = new System.Drawing.Point(284, 209);
            this.thrid_lesson.Name = "thrid_lesson";
            this.thrid_lesson.Size = new System.Drawing.Size(94, 39);
            this.thrid_lesson.TabIndex = 51;
            this.thrid_lesson.Text = "11am-12pm";
            this.thrid_lesson.UseVisualStyleBackColor = true;
            this.thrid_lesson.Click += new System.EventHandler(this.thrid_lesson_Click);
            // 
            // first_lesson
            // 
            this.first_lesson.Location = new System.Drawing.Point(84, 209);
            this.first_lesson.Name = "first_lesson";
            this.first_lesson.Size = new System.Drawing.Size(94, 39);
            this.first_lesson.TabIndex = 50;
            this.first_lesson.Text = "9am-10am";
            this.first_lesson.UseVisualStyleBackColor = true;
            this.first_lesson.Click += new System.EventHandler(this.first_lesson_Click);
            // 
            // Lesson_Date
            // 
            this.Lesson_Date.Location = new System.Drawing.Point(178, 134);
            this.Lesson_Date.Name = "Lesson_Date";
            this.Lesson_Date.Size = new System.Drawing.Size(200, 20);
            this.Lesson_Date.TabIndex = 49;
            this.Lesson_Date.ValueChanged += new System.EventHandler(this.Lesson_Date_ValueChanged);
            // 
            // submit_btn
            // 
            this.submit_btn.Location = new System.Drawing.Point(81, 413);
            this.submit_btn.Name = "submit_btn";
            this.submit_btn.Size = new System.Drawing.Size(121, 46);
            this.submit_btn.TabIndex = 47;
            this.submit_btn.Text = "Submit";
            this.submit_btn.UseVisualStyleBackColor = true;
            this.submit_btn.Click += new System.EventHandler(this.submit_btn_Click);
            // 
            // instructor_box
            // 
            this.instructor_box.FormattingEnabled = true;
            this.instructor_box.Location = new System.Drawing.Point(249, 354);
            this.instructor_box.Name = "instructor_box";
            this.instructor_box.Size = new System.Drawing.Size(121, 21);
            this.instructor_box.TabIndex = 46;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(89, 357);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(131, 13);
            this.label9.TabIndex = 45;
            this.label9.Text = "Please select an instructor";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(92, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 44;
            this.label7.Text = "Client Name";
            // 
            // eighth_lesson
            // 
            this.eighth_lesson.Location = new System.Drawing.Point(384, 254);
            this.eighth_lesson.Name = "eighth_lesson";
            this.eighth_lesson.Size = new System.Drawing.Size(94, 39);
            this.eighth_lesson.TabIndex = 60;
            this.eighth_lesson.Text = "4pm-5pm";
            this.eighth_lesson.UseVisualStyleBackColor = true;
            this.eighth_lesson.Click += new System.EventHandler(this.eighth_lesson_Click);
            // 
            // sixth_lesson
            // 
            this.sixth_lesson.Location = new System.Drawing.Point(184, 254);
            this.sixth_lesson.Name = "sixth_lesson";
            this.sixth_lesson.Size = new System.Drawing.Size(94, 39);
            this.sixth_lesson.TabIndex = 59;
            this.sixth_lesson.Text = "2pm-3pm";
            this.sixth_lesson.UseVisualStyleBackColor = true;
            this.sixth_lesson.Click += new System.EventHandler(this.sixth_lesson_Click);
            // 
            // seventh_lesson
            // 
            this.seventh_lesson.Location = new System.Drawing.Point(284, 254);
            this.seventh_lesson.Name = "seventh_lesson";
            this.seventh_lesson.Size = new System.Drawing.Size(94, 39);
            this.seventh_lesson.TabIndex = 58;
            this.seventh_lesson.Text = "3pm-4pm";
            this.seventh_lesson.UseVisualStyleBackColor = true;
            this.seventh_lesson.Click += new System.EventHandler(this.seventh_lesson_Click);
            // 
            // fifth_lesson
            // 
            this.fifth_lesson.Location = new System.Drawing.Point(84, 254);
            this.fifth_lesson.Name = "fifth_lesson";
            this.fifth_lesson.Size = new System.Drawing.Size(94, 39);
            this.fifth_lesson.TabIndex = 57;
            this.fifth_lesson.Text = "1pm-2pm";
            this.fifth_lesson.UseVisualStyleBackColor = true;
            this.fifth_lesson.Click += new System.EventHandler(this.fifth_lesson_Click);
            // 
            // LessonInfo
            // 
            this.LessonInfo.AutoSize = true;
            this.LessonInfo.Location = new System.Drawing.Point(185, 318);
            this.LessonInfo.Name = "LessonInfo";
            this.LessonInfo.Size = new System.Drawing.Size(41, 13);
            this.LessonInfo.TabIndex = 61;
            this.LessonInfo.Text = "Lesson";
            // 
            // block
            // 
            this.block.AutoSize = true;
            this.block.Location = new System.Drawing.Point(178, 91);
            this.block.Name = "block";
            this.block.Size = new System.Drawing.Size(0, 13);
            this.block.TabIndex = 62;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(209, 413);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 46);
            this.button1.TabIndex = 63;
            this.button1.Text = "Logout";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(340, 413);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(125, 46);
            this.button2.TabIndex = 64;
            this.button2.Text = "Pay my Bill";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // ClientBooking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 471);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.block);
            this.Controls.Add(this.LessonInfo);
            this.Controls.Add(this.eighth_lesson);
            this.Controls.Add(this.sixth_lesson);
            this.Controls.Add(this.seventh_lesson);
            this.Controls.Add(this.fifth_lesson);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fourth_lesson);
            this.Controls.Add(this.second_lesson);
            this.Controls.Add(this.thrid_lesson);
            this.Controls.Add(this.first_lesson);
            this.Controls.Add(this.Lesson_Date);
            this.Controls.Add(this.submit_btn);
            this.Controls.Add(this.instructor_box);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Name = "ClientBooking";
            this.Text = "ClientBooking";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button fourth_lesson;
        private System.Windows.Forms.Button second_lesson;
        private System.Windows.Forms.Button thrid_lesson;
        private System.Windows.Forms.Button first_lesson;
        private System.Windows.Forms.DateTimePicker Lesson_Date;
        private System.Windows.Forms.Button submit_btn;
        private System.Windows.Forms.ComboBox instructor_box;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button eighth_lesson;
        private System.Windows.Forms.Button sixth_lesson;
        private System.Windows.Forms.Button seventh_lesson;
        private System.Windows.Forms.Button fifth_lesson;
        private System.Windows.Forms.Label LessonInfo;
        private System.Windows.Forms.Label block;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}