﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmyMcIntyre_D3
{
    public partial class Welcome : Form
    {
        public Welcome()
        {
            InitializeComponent();
        }

        private void ClientLogin_Click(object sender, EventArgs e)
        {
            bool loggedIn = false;
            string username = "", firstname = "", lastname = "", password = "";

            //check if boxes are empty, the Trim removes white space in text from either side
            if ("".Equals(textBoxUserName.Text.Trim()) || "".Equals(textBoxPassword.Text.Trim()))
            {
                MessageBox.Show("Please make sure you enter a Username and Password");
                return;
            }

            //(1) GET the username and password from the text boxes, is good to put them in a try catch
            try
            {
                username = textBoxUserName.Text.Trim();
                password = textBoxPassword.Text.Trim();
            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            //(2) SELECT statement getting all data from users, i.e. SELECT * FROM Users
            /*YOUR CODE HERE*/

            SQL.selectQuery("SELECT * FROM CLIENT");
            //(3) IF it returns some data, THEN check each username and password combination, ELSE There are no registered users
            /*YOUR CODE HERE*/

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[0].ToString()) && password.Equals(SQL.read[1].ToString()))
                    {
                        loggedIn = true;
                        firstname = SQL.read[2].ToString();
                        lastname = SQL.read[3].ToString();
                        this.Hide();
                        ClientBooking register = new ClientBooking(firstname, lastname);
                        register.ShowDialog();
                        this.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("No Users have been registered");
                return;
            }

            //if logged in display a success message
            if (loggedIn)
            {
                //message stating we logged in good
                MessageBox.Show("Successfully logged in as: " + firstname + " " + lastname);
            }
            else
            {
                //message stating we couldn't log in
                MessageBox.Show("Login attempt unsuccessful! Please check details");
                textBoxUserName.Focus();
                return;
            }
        }

        private void ClientReg_Click(object sender, EventArgs e)
        {
            this.Hide();
            UserRegister register = new UserRegister();
            register.ShowDialog();
            this.Close();
        }

        private void Instructorpage_Click(object sender, EventArgs e)
        {
            this.Hide();
            InstructorLogin register = new InstructorLogin();
            register.ShowDialog();
            this.Close();
        }

        private void Adminpage_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminLogin register = new AdminLogin();
            register.ShowDialog();
            this.Close();
        }
    }
}
