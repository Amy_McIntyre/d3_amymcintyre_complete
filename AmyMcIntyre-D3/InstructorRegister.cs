﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmyMcIntyre_D3
{
    public partial class InstructorRegister : Form
    {
        public InstructorRegister()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string username = "", password = "", firstname = "", lastname = "", email = "", phone = "";

            try
            {
                username = textBox1.Text.Trim();
                password = textBox2.Text.Trim();
                firstname = textBox3.Text.Trim();
                lastname = textBox4.Text.Trim();
                email = textBox5.Text.Trim();
                phone = textBox6.Text.Trim();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            try
            {
                SQL.executeQuery("INSERT INTO INSTRUCTOR VALUES ('" + username + "', '" + password + "', '" + firstname + "', '" + lastname + "', '" + email + "', '" + phone + "' )");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }

            MessageBox.Show("Successfully Registered: " + firstname + " " + lastname + ". Your username is: " + username);
        }

        private void back_Button_Click(object sender, EventArgs e)
        {
            this.Hide();
            Welcome register = new Welcome();
            register.ShowDialog();
            this.Close();
        }

        private void Login_Page_Click(object sender, EventArgs e)
        {
            this.Hide();
            InstructorLogin register = new InstructorLogin();
            register.ShowDialog();
            this.Close();
        }
    }
}
