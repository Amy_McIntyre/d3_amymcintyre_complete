﻿namespace AmyMcIntyre_D3
{
    partial class AdminScheduler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LessonInfo = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Lesson_Date = new System.Windows.Forms.DateTimePicker();
            this.submit_btn = new System.Windows.Forms.Button();
            this.instructor_box = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.first_lesson = new System.Windows.Forms.Button();
            this.second_lesson = new System.Windows.Forms.Button();
            this.third_lesson = new System.Windows.Forms.Button();
            this.fourth_lesson = new System.Windows.Forms.Button();
            this.fifth_lesson = new System.Windows.Forms.Button();
            this.sixth_lesson = new System.Windows.Forms.Button();
            this.seventh_lesson = new System.Windows.Forms.Button();
            this.eighth_lesson = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LessonInfo
            // 
            this.LessonInfo.AutoSize = true;
            this.LessonInfo.Location = new System.Drawing.Point(168, 325);
            this.LessonInfo.Name = "LessonInfo";
            this.LessonInfo.Size = new System.Drawing.Size(41, 13);
            this.LessonInfo.TabIndex = 79;
            this.LessonInfo.Text = "Lesson";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(76, 325);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 74;
            this.label4.Text = "Appointment: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(239, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 73;
            this.label3.Text = "Pick a time";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 72;
            this.label2.Text = "Choose a date";
            // 
            // Lesson_Date
            // 
            this.Lesson_Date.Location = new System.Drawing.Point(161, 141);
            this.Lesson_Date.Name = "Lesson_Date";
            this.Lesson_Date.Size = new System.Drawing.Size(200, 20);
            this.Lesson_Date.TabIndex = 67;
            // 
            // submit_btn
            // 
            this.submit_btn.Location = new System.Drawing.Point(127, 431);
            this.submit_btn.Name = "submit_btn";
            this.submit_btn.Size = new System.Drawing.Size(121, 46);
            this.submit_btn.TabIndex = 66;
            this.submit_btn.Text = "Submit";
            this.submit_btn.UseVisualStyleBackColor = true;
            this.submit_btn.Click += new System.EventHandler(this.submit_btn_Click);
            // 
            // instructor_box
            // 
            this.instructor_box.FormattingEnabled = true;
            this.instructor_box.Location = new System.Drawing.Point(232, 361);
            this.instructor_box.Name = "instructor_box";
            this.instructor_box.Size = new System.Drawing.Size(121, 21);
            this.instructor_box.TabIndex = 65;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(72, 364);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(131, 13);
            this.label9.TabIndex = 64;
            this.label9.Text = "Please select an instructor";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(75, 99);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 63;
            this.label7.Text = "Client Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(81, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(363, 39);
            this.label1.TabIndex = 81;
            this.label1.Text = "Schedule an Instructor";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(161, 96);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(192, 21);
            this.comboBox1.TabIndex = 82;
            // 
            // first_lesson
            // 
            this.first_lesson.Location = new System.Drawing.Point(49, 216);
            this.first_lesson.Name = "first_lesson";
            this.first_lesson.Size = new System.Drawing.Size(102, 39);
            this.first_lesson.TabIndex = 83;
            this.first_lesson.Text = "9:00am-10:00am";
            this.first_lesson.UseVisualStyleBackColor = true;
            this.first_lesson.Click += new System.EventHandler(this.first_lesson_Click);
            // 
            // second_lesson
            // 
            this.second_lesson.Location = new System.Drawing.Point(157, 216);
            this.second_lesson.Name = "second_lesson";
            this.second_lesson.Size = new System.Drawing.Size(102, 39);
            this.second_lesson.TabIndex = 84;
            this.second_lesson.Text = "10:00am-11:00am";
            this.second_lesson.UseVisualStyleBackColor = true;
            this.second_lesson.Click += new System.EventHandler(this.second_lesson_Click);
            // 
            // third_lesson
            // 
            this.third_lesson.Location = new System.Drawing.Point(265, 216);
            this.third_lesson.Name = "third_lesson";
            this.third_lesson.Size = new System.Drawing.Size(102, 39);
            this.third_lesson.TabIndex = 85;
            this.third_lesson.Text = "11:00am-12:00am";
            this.third_lesson.UseVisualStyleBackColor = true;
            this.third_lesson.Click += new System.EventHandler(this.third_lesson_Click);
            // 
            // fourth_lesson
            // 
            this.fourth_lesson.Location = new System.Drawing.Point(373, 216);
            this.fourth_lesson.Name = "fourth_lesson";
            this.fourth_lesson.Size = new System.Drawing.Size(103, 39);
            this.fourth_lesson.TabIndex = 86;
            this.fourth_lesson.Text = "12:00pm-1:00pm";
            this.fourth_lesson.UseVisualStyleBackColor = true;
            this.fourth_lesson.Click += new System.EventHandler(this.fourth_lesson_Click);
            // 
            // fifth_lesson
            // 
            this.fifth_lesson.Location = new System.Drawing.Point(49, 261);
            this.fifth_lesson.Name = "fifth_lesson";
            this.fifth_lesson.Size = new System.Drawing.Size(102, 39);
            this.fifth_lesson.TabIndex = 87;
            this.fifth_lesson.Text = "1:00pm-2:00pm";
            this.fifth_lesson.UseVisualStyleBackColor = true;
            this.fifth_lesson.Click += new System.EventHandler(this.fifth_lesson_Click);
            // 
            // sixth_lesson
            // 
            this.sixth_lesson.Location = new System.Drawing.Point(157, 261);
            this.sixth_lesson.Name = "sixth_lesson";
            this.sixth_lesson.Size = new System.Drawing.Size(102, 39);
            this.sixth_lesson.TabIndex = 88;
            this.sixth_lesson.Text = "2:00pm-3:00pm";
            this.sixth_lesson.UseVisualStyleBackColor = true;
            this.sixth_lesson.Click += new System.EventHandler(this.sixth_lesson_Click);
            // 
            // seventh_lesson
            // 
            this.seventh_lesson.Location = new System.Drawing.Point(265, 261);
            this.seventh_lesson.Name = "seventh_lesson";
            this.seventh_lesson.Size = new System.Drawing.Size(102, 39);
            this.seventh_lesson.TabIndex = 89;
            this.seventh_lesson.Text = "3:00pm-4:00pm";
            this.seventh_lesson.UseVisualStyleBackColor = true;
            this.seventh_lesson.Click += new System.EventHandler(this.seventh_lesson_Click);
            // 
            // eighth_lesson
            // 
            this.eighth_lesson.Location = new System.Drawing.Point(375, 261);
            this.eighth_lesson.Name = "eighth_lesson";
            this.eighth_lesson.Size = new System.Drawing.Size(101, 39);
            this.eighth_lesson.TabIndex = 90;
            this.eighth_lesson.Text = "4:00pm-5:00pm";
            this.eighth_lesson.UseVisualStyleBackColor = true;
            this.eighth_lesson.Click += new System.EventHandler(this.eighth_lesson_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(265, 431);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 46);
            this.button1.TabIndex = 91;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AdminScheduler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 489);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.eighth_lesson);
            this.Controls.Add(this.seventh_lesson);
            this.Controls.Add(this.sixth_lesson);
            this.Controls.Add(this.fifth_lesson);
            this.Controls.Add(this.fourth_lesson);
            this.Controls.Add(this.third_lesson);
            this.Controls.Add(this.second_lesson);
            this.Controls.Add(this.first_lesson);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LessonInfo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Lesson_Date);
            this.Controls.Add(this.submit_btn);
            this.Controls.Add(this.instructor_box);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Name = "AdminScheduler";
            this.Text = "AdminScheduler";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label LessonInfo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker Lesson_Date;
        private System.Windows.Forms.Button submit_btn;
        private System.Windows.Forms.ComboBox instructor_box;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button first_lesson;
        private System.Windows.Forms.Button second_lesson;
        private System.Windows.Forms.Button third_lesson;
        private System.Windows.Forms.Button fourth_lesson;
        private System.Windows.Forms.Button fifth_lesson;
        private System.Windows.Forms.Button sixth_lesson;
        private System.Windows.Forms.Button seventh_lesson;
        private System.Windows.Forms.Button eighth_lesson;
        private System.Windows.Forms.Button button1;
    }
}