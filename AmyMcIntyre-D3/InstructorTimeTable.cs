﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AmyMcIntyre_D3
{
    public partial class InstructorTimeTable : Form
    {

        void stuff(string fname)
        {
            con.Open();
            cmd.CommandText = "select * from TIMESLOT where INSTRUCTORNAME like '%" + fname + "%'";
            cmd.Connection = con;
            dr = cmd.ExecuteReader();



            while (dr.Read())
            {
                listView1.View = View.Details;

                ListViewItem item = new ListViewItem(dr["clientname"].ToString());
                item.SubItems.Add(dr["time"].ToString());
                item.SubItems.Add(dr["date"].ToString());
                item.SubItems.Add(dr["instructorname"].ToString());

                listView1.Items.Add(item);
            }
            con.Close();
        }

        SqlConnection con = new SqlConnection(@"Data Source = BCS308A-05A; Database = Deliverable2_AmyMcIntyre; Integrated Security = True");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader dr;

        public static string fname;
        public static string lname;
        public static string label;
        public static string labeltext;
        public static string firstname;
        public static string lastname;
        public InstructorTimeTable(string firstname, string lastname, string labeltext)
        {
            InitializeComponent();
            fname = firstname;
            lname = lastname;
            label = labeltext;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            stuff(fname);
        }

        private void update_records_Click(object sender, EventArgs e)
        {
            this.Hide();
            InstructorBookings register = new InstructorBookings(firstname, lastname, labeltext);
            register.ShowDialog();
            this.Close();
        }

        private void delete_records_Click(object sender, EventArgs e)
        {
            this.Hide();
            Welcome register = new Welcome();
            register.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void block_request_Click(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            AppointmentInfo register = new AppointmentInfo();
            register.ShowDialog();
            this.Close();
        }
    }
}
