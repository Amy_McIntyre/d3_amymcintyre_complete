﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmyMcIntyre_D3
{
    public partial class UserRegister : Form
    {
        public UserRegister()
        {
            InitializeComponent();
        }

        private void ClientReg_Click(object sender, EventArgs e)
        {
            string username = "", password = "", firstname = "", lastname = "", email = "", phone = "", experience = "", clientcost = "", clienthours = "";

            try
            {
                username = username_box.Text.Trim();
                password = password_box.Text.Trim();
                firstname = firstname_box.Text.Trim();
                lastname = lastname_box.Text.Trim();
                email = email_box.Text.Trim();
                phone = phone_box.Text.Trim();
                experience = experiencebox.Text.Trim();
                clientcost = cost_box.Text.Trim();
                clienthours = time_box.Text.Trim();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }


            try
            {
                SQL.executeQuery("INSERT INTO CLIENT VALUES ('" + username + "', '" + password + "', '" + firstname + "', '" + lastname + "', '" + email + "', '" + phone + "', '" + experience + "', '" + clientcost + "', '" + clienthours + "'  )");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }

            MessageBox.Show("Successfully Registered: " + firstname + " " + lastname + ". Your username is: " + username);
        }

        private void LoinPage_button_Click(object sender, EventArgs e)
        {
            this.Hide();
            Welcome register = new Welcome();
            register.ShowDialog();
            this.Close();
        }

        private void Back_button_Click(object sender, EventArgs e)
        {
            this.Hide();
            Welcome register = new Welcome();
            register.ShowDialog();
            this.Close();
        }

        private void experiencebox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((string)experiencebox.SelectedItem == "Experienced")
            {
                cost_box.Text = "95.00";
                time_box.Text = "2";
            }
            else
            {
                cost_box.Text = "195.00";
                time_box.Text = "5";
            }
        }
    }
}
