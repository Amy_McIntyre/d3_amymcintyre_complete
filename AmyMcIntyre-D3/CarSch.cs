﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AmyMcIntyre_D3
{
    public partial class CarSch : Form
    {
        public void stuff()
        {
            con.Open();
            cmd.CommandText = $"select INSTRUCTORNAME, LICENSE, MAKE from CAR";
            cmd.Connection = con;
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                listview.View = View.Details;

                ListViewItem item = new ListViewItem(dr["INSTRUCTORNAME"].ToString());
                item.SubItems.Add(dr["license"].ToString());
                item.SubItems.Add(dr["make"].ToString());

                listview.Items.Add(item);
            }
            con.Close();
        }

        public static string fname;
        public static string lname;
        public static string firstname;
        public static string lastname;

        SqlConnection con = new SqlConnection(@"Data Source = BCS308A-05A; Database = Deliverable2_AmyMcIntyre; Integrated Security = True");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader dr;


        public CarSch(string firstname, string lastname)
        {
            InitializeComponent();
            string fullname;
            fname = firstname;
            lname = lastname;
            fullname = $"{firstname} {lastname}";
            instruct1.Text = fullname;

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS308A-05A;Database=Deliverable2_AmyMcIntyre;Integrated Security=True")) //CHANGE THIS IF YOU HAVE DIFFERENT DATABASE NAME
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT FNAME, LNAME FROM INSTRUCTOR", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                while (sqlReader.Read())
                {
                    instruct1.Items.Add(sqlReader["FNAME"].ToString() + " " + (sqlReader["LNAME"].ToString()));
                }
                sqlReader.Close();
            }

        }


        private void Submit_Click(object sender, EventArgs e)
        {

            string instructorname = "", make = "", license = "";

            instructorname = instruct1.Text;
            make = car1.Text;
            license = label4.Text;

            string instructorname1 = "", make1 = "", license1 = "";

            instructorname1 = instructorname;
            make1 = make;
            license1 = license;

            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=BCS308A-05A;Database=Deliverable2_AmyMcIntyre; Integrated Security = True"))
            {
                SqlCommand sqlCmd = new SqlCommand($"SELECT * FROM CAR WHERE instructorname = '{instructorname1}' AND license = '{license1}' AND make = '{make1}'", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                while (sqlReader.Read())
                {
                    if (sqlReader.HasRows)
                    {
                        instructorname = sqlReader.GetString(0);
                        license = sqlReader.GetString(1);
                        make = sqlReader.GetString(2);
                        

                        if (instructorname == "" && license == "" && make == "")
                        {

                            SQL.executeQuery("INSERT INTO CAR ( instructorname, license, make) VALUES ( '" + instructorname + "', '" + license + "','" + make + "')");
                            //success message for the user to know it worked
                            MessageBox.Show("SUCCESSFUL " + license + " at " + make + " With " + instructorname);

                        }
                        else
                        {
                            MessageBox.Show("Car or Instructor unavalible. Please Pick Another");
                            return;
                        }
                    }

                }

                sqlReader.Close();
            }
            try
            {
                SQL.executeQuery("INSERT INTO CAR (instructorname, license, make ) VALUES ('" + instructorname + "', '" + license + "', '" + make + "' )");



            }
            catch (Exception ex)
            {
                MessageBox.Show("Car Already Assigned");
                return;
            }
        }

        private void car1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((string)car1.SelectedItem == "MAZDA")
            {
                label4.Text = "JLT299";
            }
            if ((string)car1.SelectedItem == "FORD")
            {
                label4.Text = "PJK569";
            }
            if ((string)car1.SelectedItem == "HOLDEN")
            {
                label4.Text = "PZW254";
            }
            if ((string)car1.SelectedItem == "NISSAN")
            {
                label4.Text = "ISG691";
            }
            if ((string)car1.SelectedItem == "TOYOTA")
            {
                label4.Text = "LOZ182";
            }

        }

        private void load_button_Click(object sender, EventArgs e)
        {
            stuff();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string IdD = "";


            foreach (ListViewItem eachItem in listview.SelectedItems)
            {
                IdD = listview.SelectedItems[0].Text;

                SQL.executeQuery($"Delete From CAR Where INSTRUCTORNAME = '{IdD}'"); //sql query 
                listview.Items.Remove(eachItem); // deletes item from listview
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listview.Update();
            listview.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            SchoolBookings register = new SchoolBookings();
            register.ShowDialog();
            this.Close();
        }
    }
}
