﻿namespace AmyMcIntyre_D3
{
    partial class InstructorBookings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LessonInfo = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Lesson_Date = new System.Windows.Forms.DateTimePicker();
            this.submit_btn = new System.Windows.Forms.Button();
            this.instructor_box = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Lesson1 = new System.Windows.Forms.Button();
            this.Lesson2 = new System.Windows.Forms.Button();
            this.Lesson3 = new System.Windows.Forms.Button();
            this.Lesson4 = new System.Windows.Forms.Button();
            this.Lesson5 = new System.Windows.Forms.Button();
            this.Lesson6 = new System.Windows.Forms.Button();
            this.Lesson7 = new System.Windows.Forms.Button();
            this.Lesson8 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LessonInfo
            // 
            this.LessonInfo.AutoSize = true;
            this.LessonInfo.Location = new System.Drawing.Point(175, 302);
            this.LessonInfo.Name = "LessonInfo";
            this.LessonInfo.Size = new System.Drawing.Size(41, 13);
            this.LessonInfo.TabIndex = 80;
            this.LessonInfo.Text = "Lesson";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(83, 302);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 75;
            this.label4.Text = "Appointment: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(246, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 74;
            this.label3.Text = "Pick a time";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(79, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 73;
            this.label2.Text = "Choose a date";
            // 
            // Lesson_Date
            // 
            this.Lesson_Date.Location = new System.Drawing.Point(168, 118);
            this.Lesson_Date.Name = "Lesson_Date";
            this.Lesson_Date.Size = new System.Drawing.Size(200, 20);
            this.Lesson_Date.TabIndex = 68;
            // 
            // submit_btn
            // 
            this.submit_btn.Location = new System.Drawing.Point(156, 425);
            this.submit_btn.Name = "submit_btn";
            this.submit_btn.Size = new System.Drawing.Size(121, 46);
            this.submit_btn.TabIndex = 66;
            this.submit_btn.Text = "Submit";
            this.submit_btn.UseVisualStyleBackColor = true;
            this.submit_btn.Click += new System.EventHandler(this.submit_btn_Click);
            // 
            // instructor_box
            // 
            this.instructor_box.FormattingEnabled = true;
            this.instructor_box.Location = new System.Drawing.Point(239, 338);
            this.instructor_box.Name = "instructor_box";
            this.instructor_box.Size = new System.Drawing.Size(121, 21);
            this.instructor_box.TabIndex = 65;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(79, 341);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(131, 13);
            this.label9.TabIndex = 64;
            this.label9.Text = "Please select an instructor";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(82, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 63;
            this.label7.Text = "UserName";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(112, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(321, 39);
            this.label1.TabIndex = 62;
            this.label1.Text = "Block Out A Lesson";
            // 
            // Lesson1
            // 
            this.Lesson1.Location = new System.Drawing.Point(67, 194);
            this.Lesson1.Name = "Lesson1";
            this.Lesson1.Size = new System.Drawing.Size(102, 39);
            this.Lesson1.TabIndex = 81;
            this.Lesson1.Text = "9;00am-10:00am";
            this.Lesson1.UseVisualStyleBackColor = true;
            this.Lesson1.Click += new System.EventHandler(this.Lesson1_Click);
            // 
            // Lesson2
            // 
            this.Lesson2.Location = new System.Drawing.Point(175, 194);
            this.Lesson2.Name = "Lesson2";
            this.Lesson2.Size = new System.Drawing.Size(102, 39);
            this.Lesson2.TabIndex = 82;
            this.Lesson2.Text = "10:00am-11:00am";
            this.Lesson2.UseVisualStyleBackColor = true;
            this.Lesson2.Click += new System.EventHandler(this.Lesson2_Click);
            // 
            // Lesson3
            // 
            this.Lesson3.Location = new System.Drawing.Point(283, 194);
            this.Lesson3.Name = "Lesson3";
            this.Lesson3.Size = new System.Drawing.Size(102, 39);
            this.Lesson3.TabIndex = 83;
            this.Lesson3.Text = "11:00am-12:00pm";
            this.Lesson3.UseVisualStyleBackColor = true;
            this.Lesson3.Click += new System.EventHandler(this.Lesson3_Click);
            // 
            // Lesson4
            // 
            this.Lesson4.Location = new System.Drawing.Point(391, 194);
            this.Lesson4.Name = "Lesson4";
            this.Lesson4.Size = new System.Drawing.Size(102, 39);
            this.Lesson4.TabIndex = 84;
            this.Lesson4.Text = "12:00pm-1:00pm";
            this.Lesson4.UseVisualStyleBackColor = true;
            this.Lesson4.Click += new System.EventHandler(this.Lesson4_Click);
            // 
            // Lesson5
            // 
            this.Lesson5.Location = new System.Drawing.Point(67, 239);
            this.Lesson5.Name = "Lesson5";
            this.Lesson5.Size = new System.Drawing.Size(102, 39);
            this.Lesson5.TabIndex = 85;
            this.Lesson5.Text = "1:00pm-2:00pm";
            this.Lesson5.UseVisualStyleBackColor = true;
            this.Lesson5.Click += new System.EventHandler(this.Lesson5_Click);
            // 
            // Lesson6
            // 
            this.Lesson6.Location = new System.Drawing.Point(175, 239);
            this.Lesson6.Name = "Lesson6";
            this.Lesson6.Size = new System.Drawing.Size(102, 39);
            this.Lesson6.TabIndex = 86;
            this.Lesson6.Text = "2:00pm-3:00pm";
            this.Lesson6.UseVisualStyleBackColor = true;
            this.Lesson6.Click += new System.EventHandler(this.Lesson6_Click);
            // 
            // Lesson7
            // 
            this.Lesson7.Location = new System.Drawing.Point(283, 239);
            this.Lesson7.Name = "Lesson7";
            this.Lesson7.Size = new System.Drawing.Size(102, 39);
            this.Lesson7.TabIndex = 87;
            this.Lesson7.Text = "3:00pm-4:00pm";
            this.Lesson7.UseVisualStyleBackColor = true;
            this.Lesson7.Click += new System.EventHandler(this.Lesson7_Click);
            // 
            // Lesson8
            // 
            this.Lesson8.Location = new System.Drawing.Point(391, 239);
            this.Lesson8.Name = "Lesson8";
            this.Lesson8.Size = new System.Drawing.Size(102, 39);
            this.Lesson8.TabIndex = 88;
            this.Lesson8.Text = "4:00pm-5:00pm";
            this.Lesson8.UseVisualStyleBackColor = true;
            this.Lesson8.Click += new System.EventHandler(this.Lesson8_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Blocked Out"});
            this.comboBox1.Location = new System.Drawing.Point(175, 76);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 89;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(283, 425);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 46);
            this.button1.TabIndex = 90;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // InstructorBookings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 483);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.Lesson8);
            this.Controls.Add(this.Lesson7);
            this.Controls.Add(this.Lesson6);
            this.Controls.Add(this.Lesson5);
            this.Controls.Add(this.Lesson4);
            this.Controls.Add(this.Lesson3);
            this.Controls.Add(this.Lesson2);
            this.Controls.Add(this.Lesson1);
            this.Controls.Add(this.LessonInfo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Lesson_Date);
            this.Controls.Add(this.submit_btn);
            this.Controls.Add(this.instructor_box);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Name = "InstructorBookings";
            this.Text = "InstructorBookings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LessonInfo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker Lesson_Date;
        private System.Windows.Forms.Button submit_btn;
        private System.Windows.Forms.ComboBox instructor_box;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Lesson1;
        private System.Windows.Forms.Button Lesson2;
        private System.Windows.Forms.Button Lesson3;
        private System.Windows.Forms.Button Lesson4;
        private System.Windows.Forms.Button Lesson5;
        private System.Windows.Forms.Button Lesson6;
        private System.Windows.Forms.Button Lesson7;
        private System.Windows.Forms.Button Lesson8;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button1;
    }
}